#
# Copyright (C) 2011 by Alfred Krohmer <devkid>
#
# Little script for flashing a program to a myAVR SmartControl Board via the
# bootloader located there.
#
# Licensed under GNU GPLv3, see http://devkid.net/ for more information
#

import serial
import select
import time
import sys
import subprocess

def wait_silly ():
	# bug? receiving silly bytes
	while fd.inWaiting () > 0:
		fd.flushInput ()
		if fd.inWaiting () == 0:
			time.sleep (0.1)

def failure ():
	print ("Failure.")
	quit (1)

def read_field (fd):
	s = ""
	
	while True:
		x = fd.read (1)
		if x == '\r':
			break
		s += x
	
	return s

fd = serial.Serial ("/dev/ttyUSB0", 19200, bytesize = serial.EIGHTBITS, stopbits = serial.STOPBITS_ONE, parity = serial.PARITY_NONE)

print "Connecting to chip, press the RESET button!"

tries = 0

while True:
	# intro
	fd.write (chr (0xAB) + "myBurn" + chr (0xBB))
	fd.flush ()
	
	i,o,e = select.select ([fd], [], [fd], 0.1)
	
	sys.stdout.write (".")
	sys.stdout.flush ()
	
	if len (e) > 0:
		failure ()
	
	if len (i) > 0 and fd.read (7) == "myBurn\r":
		break
	
	tries += 1
	if tries > 50:
		print ""
		failure ()

print ""
print ""

code = file (sys.argv [1]).read ()

print "Connected successfully, hardware info:"

# description
hw    = read_field (fd)
hwver = read_field (fd)
swver = read_field (fd)
proc  = read_field (fd)
freq  = int (read_field (fd), 16)
blks  = int (read_field (fd), 16)
maxs  = int (read_field (fd), 16)
baud  = int (read_field (fd), 16)

if fd.read (1) <> "\n":
	failure ()

fd.baudrate = baud

print "Hardware:         " + hw
print "Hardware version: " + hwver
print "Software version: " + swver
print "Processor:        " + proc
print "Processor clock:  " + str (freq)
print "Block size:       " + str (blks)
print "Max. size:        " + str (maxs)
print "Baudrate:         " + str (baud)

print ""

time.sleep (0.5)

wait_silly ()

print "Writing program..."

fd.write ("B" + chr (0x00) + chr (0x00) + chr (len (code) & 0x00ff) + chr ((len (code) & 0xff00) >> 8) + "\n")
fd.flush ()

if fd.read (2) <> "B\n":
	failure ()

for i in range (0, len (code), blks):
	sys.stdout.write (".")
	sys.stdout.flush ()
	l = (blks if (i + blks <= len (code)) else (len (code) - i))
	fd.write ("b" + chr (l) + "\n" + code [i:i+l])
	time.sleep (0.1)
	if fd.read (3) <> "b0\n":
		failure ()
fd.write ("e\n")

if fd.read (2) <> "e0":
	failure ()

checksum = int (fd.read (10), 16)

# quit
fd.write ("X\n")
fd.flush ()

if fd.read (3) <> "\nX\n":
	failure ()

print ""

while True:
	if fd.inWaiting () > 0:
		fd.flushInput ()
		time.sleep (0.1)
	else:
		break

print "Finished writing!"

